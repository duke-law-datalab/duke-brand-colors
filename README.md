# Duke Brand Colors
R Implementation of Duke Brand Colors

https://brand.duke.edu/colors/

## Colors
![Colors](img/palette.png)

## Palette List: `duke_colors`
- blues
- neutrals
- accents
- full

## Functions
### ggplot2 scales
- scale_fill_duke_c(name = "blues|full|accents|neutrals")
- scale_fill_duke_d(name = "blues|full|accents|neutrals")
- scale_color_duke_c(name = "blues|full|accents|neutrals")
- scale_color_duke_d(name = "blues|full|accents|neutrals")

### Palette Function
`duke_palettes(name, type = c("discrete", "continuous"))`


## Color Information: `dbc`
```r
$duke_navy_blue
[1] "#012169"

$copper
[1] "#C84E00"

$persimmon
[1] "#E89923"

$dandelion
[1] "#FFD960"

$piedmont
[1] "#A1B80D"

$eno
[1] "#339898"

$magnolia
[1] "#1D6363"

$prussian_blue
[1] "#005587"

$duke_royal_blue
[1] "#005398"

$shale_blue
[1] "#0577B1"

$ironweed
[1] "#993399"

$hatteras
[1] "#E2E6ED"

$whisper_gray
[1] "#F3F2F1"

$ginger_beer
[1] "#FCF7E5"

$dogwood
[1] "#988675"

$shackleford
[1] "#DAD0C6"

$cast_iron
[1] "#262626"

$graphite
[1] "#666666"

$granite
[1] "#B5B5B5"

$limestone
[1] "#E5E5E5"
```